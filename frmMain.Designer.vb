<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
Me.txtDays = New System.Windows.Forms.TextBox
Me.txtFolder = New System.Windows.Forms.TextBox
Me.lblFolder = New System.Windows.Forms.Label
Me.lblDays = New System.Windows.Forms.Label
Me.cmdDelete = New System.Windows.Forms.Button
Me.diaFolder = New System.Windows.Forms.FolderBrowserDialog
Me.SuspendLayout()
'
'txtDays
'
Me.txtDays.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
Me.txtDays.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.txtDays.Location = New System.Drawing.Point(15, 163)
Me.txtDays.Name = "txtDays"
Me.txtDays.Size = New System.Drawing.Size(234, 26)
Me.txtDays.TabIndex = 0
Me.txtDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
'
'txtFolder
'
Me.txtFolder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
Me.txtFolder.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.txtFolder.Location = New System.Drawing.Point(15, 43)
Me.txtFolder.Multiline = True
Me.txtFolder.Name = "txtFolder"
Me.txtFolder.Size = New System.Drawing.Size(423, 81)
Me.txtFolder.TabIndex = 1
Me.txtFolder.TabStop = False
'
'lblFolder
'
Me.lblFolder.AutoSize = True
Me.lblFolder.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.lblFolder.Location = New System.Drawing.Point(12, 20)
Me.lblFolder.Name = "lblFolder"
Me.lblFolder.Size = New System.Drawing.Size(182, 20)
Me.lblFolder.TabIndex = 2
Me.lblFolder.Text = "Delete file(s) from folder:"
'
'lblDays
'
Me.lblDays.AutoSize = True
Me.lblDays.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.lblDays.Location = New System.Drawing.Point(12, 140)
Me.lblDays.Name = "lblDays"
Me.lblDays.Size = New System.Drawing.Size(237, 20)
Me.lblDays.TabIndex = 3
Me.lblDays.Text = "Which are more than X days old:"
'
'cmdDelete
'
Me.cmdDelete.Enabled = False
Me.cmdDelete.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.cmdDelete.Location = New System.Drawing.Point(343, 153)
Me.cmdDelete.Name = "cmdDelete"
Me.cmdDelete.Size = New System.Drawing.Size(95, 36)
Me.cmdDelete.TabIndex = 1
Me.cmdDelete.Text = "delete"
Me.cmdDelete.UseVisualStyleBackColor = True
'
'frmMain
'
Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
Me.ClientSize = New System.Drawing.Size(455, 212)
Me.Controls.Add(Me.cmdDelete)
Me.Controls.Add(Me.lblDays)
Me.Controls.Add(Me.lblFolder)
Me.Controls.Add(Me.txtFolder)
Me.Controls.Add(Me.txtDays)
Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
Me.Margin = New System.Windows.Forms.Padding(4)
Me.MaximizeBox = False
Me.Name = "frmMain"
Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
Me.Text = " Delete files older than X days"
Me.ResumeLayout(False)
Me.PerformLayout()

End Sub
    Friend WithEvents txtDays As System.Windows.Forms.TextBox
    Friend WithEvents txtFolder As System.Windows.Forms.TextBox
    Friend WithEvents lblFolder As System.Windows.Forms.Label
    Friend WithEvents lblDays As System.Windows.Forms.Label
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents diaFolder As System.Windows.Forms.FolderBrowserDialog

End Class
