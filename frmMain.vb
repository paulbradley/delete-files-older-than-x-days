' See LICENSE file
' Copyright (c) 213 Paul Bradley
' (https://paulbradley.org)

Public Class frmMain

    Public Const frmMainTitle As String = " Delete files older than X days"

    Private Sub txtFolder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFolder.Click
        ShowFolderBrowseDialog()
    End Sub

    Private Sub ShowFolderBrowseDialog()

        diaFolder.ShowNewFolderButton = False
        diaFolder.Description = "Please select the desired folder to delete files from."

        If Len(txtFolder.Text) = 0 Then
            diaFolder.RootFolder = Environment.SpecialFolder.Desktop
        Else
            diaFolder.SelectedPath = txtFolder.Text
        End If

        diaFolder.ShowDialog()

        If Len(diaFolder.SelectedPath) > 0 Then
            txtFolder.Text = diaFolder.SelectedPath
        Else
            txtFolder.Text = ""
        End If

        txtDays.Focus()

    End Sub

    Private Function DeleteFilesXDaysOld( _
        ByVal Path As String, _
        Optional ByVal Pattern As String = "*.*", _
        Optional ByVal Days As Integer = 365) As Long()

        ' ReturnValues is an array which holds 2 values
        ' The first being the count of files deleted
        ' The second being the amount of space freed
        Dim ReturnValues = New Long() {0,0}

        Dim File     As String
        Dim FileInfo As System.IO.FileInfo
        Dim Files()  As String = System.IO.Directory.GetFiles(Path, Pattern)

        Try
            For Each File In Files
                FileInfo = New System.IO.FileInfo(File)

                If FileInfo.LastWriteTime < Today.AddDays(0 - Days) Then
                    Me.Text = " deleting " & FileInfo.Name()
                    ReturnValues(0) += 1
                    ReturnValues(1) += FileInfo.Length()
                    FileInfo.Delete()
                End If
            Next

            Return ReturnValues

        Catch exn As Exception
            MsgBox(exn.ToString, _
                MsgBoxStyle.Critical Or _
                MsgBoxStyle.OkOnly Or _
                MsgBoxStyle.SystemModal, _
                "Error")
            Return ReturnValues
        End Try
    End Function

    Private Function FormatFileSize( _
        ByVal FileSizeBytes As Long) As String

        Dim sizeTypes() As String  = {"bytes", "Kb", "Mb", "Gb"}
        Dim Len         As Decimal = FileSizeBytes
        Dim sizeType    As Integer = 0

        Do While Len > 1024
            Len = Decimal.Round(Len / 1024, 2)
            sizeType += 1
            If sizeType >= sizeTypes.Length - 1 Then Exit Do
        Loop

        Dim Resp As String = Len.ToString _
            & " " & sizeTypes(sizeType)

        Return Resp
    End Function

    Private Sub frmMain_Load( ByVal sender As System.Object,  ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = frmMainTitle
        cmdDelete.Enabled = False
        txtDays.Focus()
    End Sub

    Private Sub txtDays_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDays.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9")) And _
            Asc(e.KeyChar) <> Keys.Back Then
            Beep()
            e.Handled = True
        End If
    End Sub

    Private Sub txtDays_TextChanged( ByVal sender As System.Object,  ByVal e As System.EventArgs) Handles txtDays.TextChanged
        CanContinue()
    End Sub

    Private Sub cmdDelete_Click( ByVal sender As System.Object,  ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim intReturn       As Integer
        Dim ReturnedValues  As Long()

        Try

            intReturn = MessageBox.Show( _
                "Are you sure you want to delete files in" _
                & Environment.NewLine _
                & txtFolder.Text _
                & Environment.NewLine _
                & Environment.NewLine _
                & "which are older than " & txtDays.Text & " days?", _
                "Are You Sure?", _
                MessageBoxButtons.YesNo, _
                MessageBoxIcon.Question)

            If intReturn <> 6 Then
                Exit Sub
            Else
                Me.Cursor = Cursors.WaitCursor

                ReturnedValues = DeleteFilesXDaysOld(txtFolder.Text, "*.*", CInt(txtDays.Text))

                Me.Text = frmMainTitle
                Me.Cursor = Cursors.Arrow

                MessageBox.Show( _
                    ReturnedValues(0) & " files deleted freeing " _
                    & FormatFileSize(ReturnedValues(1)), _
                    "Information", _
                    MessageBoxButtons.OK, _
                    MessageBoxIcon.Information)

            End If

        Catch exn As Exception
            Me.Cursor = Cursors.Arrow

            MsgBox(exn.ToString, _
                MsgBoxStyle.Critical Or _
                MsgBoxStyle.OkOnly Or _
                MsgBoxStyle.SystemModal, _
                "Error")
        End Try
    End Sub

    Private Sub CanContinue()
        cmdDelete.Enabled = False
        If txtDays.Text.Length > 0 And _
            txtFolder.Text.Length > 0 Then cmdDelete.Enabled = True
    End Sub

    Private Sub txtFolder_TextChanged( ByVal sender As System.Object,  ByVal e As System.EventArgs) Handles txtFolder.TextChanged
        CanContinue()
    End Sub

End Class